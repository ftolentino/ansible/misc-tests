# Ansible Misc Tests

Some miscellaneous tests with Ansible.

## Usage

Run the test playbooks inside `playbooks/`. Example:

```console
$ ansible-playbook [options] playbooks/conditional-bare-variables.yml
```

You can use the [helper script](run.sh) to run against localhost:

```console
$ ./run.sh playbooks/conditional-bare-variables.yml
```

## License

[WTFPL](LICENSE).
