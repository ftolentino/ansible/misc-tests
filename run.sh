#!/bin/sh

set -ex

ansible-playbook -c local -i inventory.ini -v "$@"
